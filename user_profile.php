
<?php
// Connect to database (replace with your database credentials)

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "user_db";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check for upcoming appointments
$current_date = date("Y-m-d"); // Get current date
$sql = "SELECT * FROM bookings WHERE appointment_date > '$current_date' ORDER BY appointment_date ASC LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // Display upcoming appointment date
  $row = $result->fetch_assoc();
  $appointment_date = $row["appointment_date"];
  echo '<div class="container">';
  echo "<h1>Your next vaccination appointment is on $appointment_date.</h1>";
  echo '<a href="index.php" class="button">Log out</a>';
  echo '</div>';
   
}

$conn->close();


?>



<style>
    body{
        background-color: #07A2D8;
    }
    .container {
      display: grid;
      max-width: 1000px; 
      margin: 0 auto; 
      height: 50vh;
      place-items: center;
      color: #0b254f;
      font-family: Poppins, sans-serif;
      background-color: #ffffff;
      border-radius: 30px;
 
    }
   .button{
    margin-top: 3px;
    width: 30%;
    background-color: #07A2D8;
    color: #080710;
    padding: 15px 0;
    font-size: 18px;
    font-weight: 600;
    border-radius: 5px;
    cursor: pointer;
    text-align: center;
    text-decoration: none;
    
}

.button:hover{
  background-color: #F57E57;
}
    </style>